/************************************************************************//**
 * \brief: BATE - Bot Agent for Telegram on ESP platforms.
 *
 * This is a simple Telegram bot implementation using esp_http_client API.
 *
 * \note Functions in this module are not reentrant.
 * \note cJSON pointers filled in structures, are invalidated if the root
 * json is deleted. Other members continue being valid.
 ****************************************************************************/

#ifndef _BATE_H_
#define _BATE_H_

#include <esp_http_client.h>
#include "json_util.h"

/// Information extracted from the JSON returned by bate_me_get()
#define BATE_BOT_INFO_TABLE(X_MACRO)		\
	X_MACRO(id,         long,  long)	\
	X_MACRO(first_name, char*, string)	\
	X_MACRO(username,   char*, string)

/// Information relative to a message from an update JSON
#define BATE_MESSAGE_TABLE(X_MACRO)			\
	X_MACRO(message_id,       long,   long)		\
	X_MACRO(from,             cJSON*, object)	\
	X_MACRO(date,             long,   long)		\
	X_MACRO(chat,             cJSON*, object)	\
	X_MACRO(reply_to_message, cJSON*, object)	\
	X_MACRO(text,             char*,  string)	\
	X_MACRO(entities,         cJSON*, array)

/// From information relative to a message from JSON
#define BATE_FROM_TABLE(X_MACRO)		\
	X_MACRO(id,            long,  long)	\
	X_MACRO(is_bot,        bool,  bool)	\
	X_MACRO(first_name,    char*, string)	\
	X_MACRO(last_name,     char*, string)	\
	X_MACRO(username,      char*, string)	\
	X_MACRO(language_code, char*, string)

/// Chat information extracted from a message chat JSON
#define BATE_CHAT_TABLE(X_MACRO)		\
	X_MACRO(id,         long,  long)	\
	X_MACRO(type,       char*, string)	\
	X_MACRO(username,   char*, string)	\
	X_MACRO(first_name, char*, string)	\
	X_MACRO(last_name,  char*, string)

/// Entity information extracted from a message entity JSON
#define BATE_ENTITY_TABLE(X_MACRO)		\
	X_MACRO(type,   char*, string)		\
	X_MACRO(offset, long,  long)		\
	X_MACRO(length, long,  long)

/// Callback query information extracted from an update
#define BATE_CALLBACK_QUERY_TABLE(X_MACRO)		\
	X_MACRO(id,                char*,  string)	\
	X_MACRO(from,              cJSON*, object)	\
	X_MACRO(message,           cJSON*, object)	\
	X_MACRO(inline_message_id, char*,  string)	\
	X_MACRO(chat_instance,     char*,  string)	\
	X_MACRO(data,              char*,  string)

/// Helper X-Macro to extract tables above as structs
#define X_AS_STRUCT(name, type_decl, type_name)		\
	type_decl name;

/// Telegram bot related information structure
struct bate_bot_info {
	BATE_BOT_INFO_TABLE(X_AS_STRUCT);
};

/// Update types
enum bate_update_type {
	BATE_UPDATE_TYPE_UNSUPPORTED = 0,	///< Unsupported update
	BATE_UPDATE_TYPE_MESSAGE,		///< Message update
	BATE_UPDATE_TYPE_CALLBACK_QUERY,	///< Callback query update
	BATE_UPDATE_TYPE_MAX			///< Number of update types
};

/// Update data
struct bate_update {
	enum bate_update_type type;
	long update_id;
	union {
		cJSON *message;
		cJSON *callback_query;
	};
};

/// Message data
struct bate_message {
	BATE_MESSAGE_TABLE(X_AS_STRUCT);
};

/// From data
struct bate_from {
	BATE_FROM_TABLE(X_AS_STRUCT);
};

/// Chat data
struct bate_chat {
	BATE_CHAT_TABLE(X_AS_STRUCT);
};

/// Optional entity data
struct bate_entity {
	BATE_ENTITY_TABLE(X_AS_STRUCT);
};

/// Callback query data
struct bate_callback_query {
	BATE_CALLBACK_QUERY_TABLE(X_AS_STRUCT);
};

/// Supported parse modes for text messages
enum bate_text_mode {
	BATE_TEXT_PLAIN = 0,	///< Plain text
	BATE_TEXT_HTML,		///< HTML formatted text
	BATE_TEXT_MARKDOWN,	///< Markdown formatted text
	BATE_TEXT_NUM_MODES	///< Number of text modes
};

/// Supported file types for the bate_file_send() function
enum bate_file_type {
	BATE_FILE_PHOTO = 0,	///< Photograph
	BATE_FILE_AUDIO,	///< Audio file
	BATE_FILE_DOCUMENT,	///< Generic document
	BATE_FILE_VIDEO,	///< Video file
	BATE_FILE_ANIMATION,	///< Animation (GIF or MP4 without sound)
	BATE_FILE_VOICE,	///< Voice file
	BATE_FILE_VIDEO_NOTE,	///< Video note
	BATE_FILE_MEDIA_GROUP,	///< Group of media elements
	BATE_FILE_MAX		///< Number of supported file types
};

/// Mime types
enum bate_mime_type {
	BATE_MIME_TEXT_PLAIN = 0,	///< text/plain
	BATE_MIME_TEXT_HTML,		///< text/html
	BATE_MIME_IMAGE_JPG,		///< image/jpeg
	BATE_MIME_IMAGE_PNG,		///< image/png
	BATE_MIME_IMAGE_GIF,		///< image/gif
	BATE_MIME_IMAGE_BMP,		///< image/bmp
	BATE_MIME_VIDEO_XMSVIDEO,	///< video/x-msvideo (AVI)
	BATE_MIME_VIDEO_MP4,		///< video/mp4
	BATE_MIME_AUDIO_XWAV,		///< audio/x-wav
	BATE_MIME_AUDIO_XMPEG3,		///< audio/x-mpeg-3
	BATE_MIME_APPLICATION_XBINARY,	///< application/x-binary
	BATE_MIME_APPLICATION_JSON,	///< application/json
	BATE_MIME_MAX
};

/// Table with the different types of inline keyboard buttons
#define INLINE_KEYB_BTN_TYPE_TABLE(X_MACRO)				       \
	X_MACRO(URL,                 string, const char*, url)		       \
	X_MACRO(CALLBACK_DATA,       string, const char*, callback_data)       \
	X_MACRO(SWITCH_INLINE_QUERY, string, const char*, switch_inline_query) \
	X_MACRO(SWITCH_INLINE_QUERY_CURRENT_CHAT, string, const char*,         \
			switch_inline_query_current_chat)                      \
	X_MACRO(PAY,                  bool,   bool,  pay)

/// Expand table as inline keyboard button type enum
#define X_AS_INLINE_KEYB_TYPE_ENUM(u_name, type_name, type_decl, l_name) \
	BATE_INLINE_KEYB_TYPE_ ## u_name,

/// Supported keyboard button types
enum bate_inline_keyboard_button_type {
	INLINE_KEYB_BTN_TYPE_TABLE(X_AS_INLINE_KEYB_TYPE_ENUM)
	BATE_INLINE_KEYB_TYPE_MAX
};

/// Expand table as inline keyboard button type union
#define X_AS_INLINE_KEYB_UNION(u_name, type_name, type_decl, l_name)	 \
	type_decl l_name;

/// Inline keyboard button data structure
struct bate_inline_keyboard_button {
	enum bate_inline_keyboard_button_type type;
	const char *text;
	union {
		INLINE_KEYB_BTN_TYPE_TABLE(X_AS_INLINE_KEYB_UNION)
	};
};

/// Keboard flags for bate_keyboard_reply(). Can be ORed
enum bate_keyboard_flags {
	BATE_KEYB_NO_FLAGS = 0,	///< No flags
	BATE_KEYB_RESIZE = 1,	///< Resize for optimal fit
	BATE_KEYB_ONE_TIME = 2,	///< Hide as soon as it is used
	BATE_KEYB_SELECTIVE = 4	///< Target selected users (see docs)
};

/************************************************************************//**
 * \brief Generic GET request against the Telegram bot API.
 *
 *  The method can have parameters. E.g.: "getUpdates?timeout=60". It is
 *  not usually needed to directly use this function. Use specialized ones
 *  such as bate_me_get() or bate_msg_send().
 *
 * \param[in]  method Method with optional parameters when required. Must
 *             be null terminated. E.g. "getUpdates".
 * \param[out] reply  JSON object with the reply, NULL on error.
 *
 * \return HTTP status code or -1 if the request was not completed.
 ****************************************************************************/
int bate_get(const char *method, cJSON **reply);

/************************************************************************//**
 * \brief Generic GET POST request against the Telegram bot API.
 *
 *  It is not usually needed to directly use this function. Use specialized
 *  ones such as bate_file_send().
 *
 * \param[in]  method      Method with optional parameters when required. Must
 *             be null terminated. E.g. "sendPhoto".
 * \param[in]  data         Data to send in the POST body.
 * \param[in]  length       Length of the data to send.
 * \param[in]  content_type Content-Type HTTP header for the data payload.
 * \param[out] reply        JSON object with the reply, NULL on error.
 *
 * \return HTTP status code or -1 if the request was not completed.
 ****************************************************************************/
int bate_post(const char *method, const char *data, int length,
		const char *content_type, cJSON **reply);

/************************************************************************//**
 * \brief Send a file to the Telegram bot.
 *
 *  Send a file, allowing different send methods: the file can be sent as
 *  a photo, an audio, a document, a video, etc.
 *
 * \param[in] chat_id  The chat identifier of the bot user.
 * \param[in] data     Pointer to the file raw data.
 * \param[in] length   Byte length of the file data.
 * \param[in] filename Name of the file to report to the Telegram API.
 * \param[in] type     File type from the bate_file_type enum.
 * \param[in] mime     Mime type of the file from the bate_mime_type enum.
 * \param[out] reply   JSON object with the reply, NULL on error.
 *
 * \return HTTP status code or -1 if the request was not completed.
 ****************************************************************************/
int bate_file_send(const char *chat_id, const char *data, size_t length,
		const char *filename, enum bate_file_type type,
		enum bate_mime_type mime, cJSON **reply);

/************************************************************************//**
 * \brief Get bot data.
 *
 *  Obtains data needed to identify the bot.
 *
 * \param[out] reply   JSON object with the reply, NULL on error.
 *
 * \return HTTP status code or -1 if the request was not completed.
 ****************************************************************************/
static inline int bate_me_get(cJSON **reply)
{
	return bate_get("getMe", reply);
}

/************************************************************************//**
 * \brief Parse bot data.
 *
 *  Parses a bot data JSON object, filling the bot_data structure.
 *
 * \param json JSON object with the data obtained from a bate_me_get() call.
 *
 * \return Structure with the bot data.
 ****************************************************************************/
struct bate_bot_info *bate_me_parse(const cJSON *json);

/************************************************************************//**
 * \brief Free a bot_data structure.
 *
 * \param data Bot data to free.
 ****************************************************************************/
void bate_me_free(struct bate_bot_info *data);

/************************************************************************//**
 * \brief Get message updates.
 *
 *  Gets new messages received by the bot since the last call.
 *
 * \param[in] timeout_s Timeout in seconds (for the long poll request).
 * \param[in] offset    Message id from which to start receiving updates.
 * \param[out] reply    JSON object with the reply, NULL on error.
 *
 * \return JSON object with the updates, or NULL on error.
 * \note Returned object must be freed with cJSON_Delete() when no longer
 *       needed.
 ****************************************************************************/
int bate_updates_get(long offet, unsigned int timeout_s, cJSON **reply);

/************************************************************************//**
 * \brief Parse an update JSON and get the results.
 *
 *  Parses a JSON received after a call to bate_updates_get(), and returns
 *  the results as a bate_update structure.
 *
 * \param[in] update JSON update object to parse.
 *
 * \return bate_update structure with the data corresponding to the JSON
 * input.
 * \note Returned data must be freed by calling bate_update_free() when no
 * longer needed.
 ****************************************************************************/
struct bate_update *bate_update_parse(const cJSON *update);

/************************************************************************//**
 * \brief Free a bate_update structure.
 *
 *  Free the memory used by a bate_update data structure, previously obtained
 *  by a call to bate_update_parse() function.
 *
 * \param[in] data Data to free.
 ****************************************************************************/
void bate_update_free(struct bate_update *data);

/************************************************************************//**
 * \brief Parse a message JSON and get the results.
 *
 *  Parses a message JSON from a bate_update structure and returns the
 *  results as a bate_message structure.
 *
 * \param[in] message JSON message object to parse.
 *
 * \return bate_message structure with the data corresponding to the JSON
 * input.
 * \note Returned data must be freed by calling bate_message_free() when no
 * longer needed.
 ****************************************************************************/
struct bate_message *bate_message_parse(const cJSON *message);

/************************************************************************//**
 * \brief Free a bate_message structure.
 *
 *  Free the memory used by a bate_message data structure, previously obtained
 *  by a call to bate_message_parse() function.
 *
 * \param[in] data Data to free.
 ****************************************************************************/
void bate_message_free(struct bate_message *data);

/************************************************************************//**
 * \brief Parse a from JSON and get the results.
 *
 *  Parses a from JSON from a bate_message structure and returns the
 *  results as a bate_from structure.
 *
 * \param[in] from JSON from object to parse.
 *
 * \return bate_from structure with the data corresponding to the JSON
 * input.
 * \note Returned data must be freed by calling bate_from_free() when no
 * longer needed.
 ****************************************************************************/
struct bate_from *bate_from_parse(const cJSON *from);

/************************************************************************//**
 * \brief Free a bate_from structure.
 *
 *  Free the memory used by a bate_from data structure, previously obtained
 *  by a call to bate_from_parse() function.
 *
 * \param[in] data Data to free.
 ****************************************************************************/
void bate_from_free(struct bate_from *data);

/************************************************************************//**
 * \brief Parse a chat JSON and get the results.
 *
 *  Parses a chat JSON from a bate_message structure and returns the
 *  results as a bate_chat structure.
 *
 * \param[in] chat JSON chat object to parse.
 *
 * \return bate_chat structure with the data corresponding to the JSON
 * input.
 * \note Returned data must be freed by calling bate_chat_free() when no
 * longer needed.
 ****************************************************************************/
struct bate_chat *bate_chat_parse(const cJSON *chat);

/************************************************************************//**
 * \brief Free a bate_chat structure.
 *
 *  Free the memory used by a bate_chat data structure, previously obtained
 *  by a call to bate_chat_parse() function.
 *
 * \param[in] data Data to free.
 ****************************************************************************/
void bate_chat_free(struct bate_chat *data);

/************************************************************************//**
 * \brief Parse an entity JSON and get the results.
 *
 *  Parses an entity JSON from a bate_message structure and returns the
 *  results as a bate_entity structure.
 *
 * \param[in] entity JSON entity object to parse.
 *
 * \return bate_entity structure with the data corresponding to the JSON
 * input.
 * \note Returned data must be freed by calling bate_entity_free() when no
 * longer needed.
 ****************************************************************************/
struct bate_entity *bate_entity_parse(const cJSON *entity);

/************************************************************************//**
 * \brief Free a bate_entity structure.
 *
 *  Free the memory used by a bate_entity data structure, previously obtained
 *  by a call to bate_entity_parse() function.
 *
 * \param[in] data Data to free.
 ****************************************************************************/
void bate_entity_free(struct bate_entity *data);

/************************************************************************//**
 * \brief Parse a callback_query JSON and get the results.
 *
 *  Parses a callback_query JSON from a bate_update structure and returns the
 *  results as a bate_callback_query structure.
 *
 * \param[in] entity JSON entity object to parse.
 *
 * \return bate_callback_query structure with the data corresponding to
 * the JSON input.
 * \note Returned data must be freed by calling bate_callback_query_free()
 * when no longer needed.
 ****************************************************************************/
struct bate_callback_query *bate_callback_query_parse(const cJSON *entity);

/************************************************************************//**
 * \brief Free a bate_callback_query structure.
 *
 *  Free the memory used by a bate_callback_query data structure, previously
 *  obtained by a call to bate_callback_query_parse() function.
 *
 * \param[in] data Data to free.
 ****************************************************************************/
void bate_callback_query_free(struct bate_callback_query *data);

/************************************************************************//**
 * \brief Send message to chat.
 *
 *  Sends a message to the specified chat_id, with optional markup.
 *
 * \param[in]  chat_id Chat identifier to send the message to.
 * \param[in]  text Text to send in the message.
 * \param[in]  parse_mode Set to BATE_TEXT_HTML or BATE_TEXT_MARKDOWN if
 *             you want HTML or Markdown format parsing.
 * \param[out] reply JSON object with the reply, NULL on error.
 *
 * \return HTTP status code or -1 if the request was not completed.
 ****************************************************************************/
int bate_msg_send(const char *chat_id, const char *text,
		enum bate_text_mode parse_mode, cJSON **reply);

/************************************************************************//**
 * \brief Send message to chat, also including a keyboard.
 *
 *  Sends a message to the specified chat_id, with optional markup. A
 *  keyboard layout is also sent, for the user to be able to easily send
 *  messages by using the keys.
 *
 * \param[in]  chat_id Chat identifier to send the message to.
 * \param[in]  text Text to send in the message.
 * \param[in]  parse_mode Set to BATE_TEXT_HTML or BATE_TEXT_MARKDOWN if
 *             you want HTML or Markdown format parsing.
 * \param[in]  key Text for the different keyboard keys.
 * \param[in]  rows Number of rows of the keyboard.
 * \param[in]  cols Number of colums for each keyboard row.
 * \param[in]  flags Keyboard optional flags.
 * \param[out] reply JSON object with the reply, NULL on error.
 *
 * \return HTTP status code or -1 if the request was not completed.
 ****************************************************************************/
int bate_keyboard_reply(const char *chat_id, const char *text,
		enum bate_text_mode parse_mode, const char **key,
		unsigned int rows, const unsigned int *cols,
		enum bate_keyboard_flags flags, cJSON **reply);

/************************************************************************//**
 * \brief Send message to chat, also including an inline keyboard.
 *
 *  Sends a message to the specified chat_id, with optional markup. An
 *  inline keyboard layout is also sent, for the user to be able to select
 *  different actions depending on the sent buttons.
 *
 * \param[in]  chat_id Chat identifier to send the message to.
 * \param[in]  text Text to send in the message.
 * \param[in]  parse_mode Set to BATE_TEXT_HTML or BATE_TEXT_MARKDOWN if
 *             you want HTML or Markdown format parsing.
 * \param[in]  key Buttons for the different keys.
 * \param[in]  rows Number of rows of the keyboard.
 * \param[in]  cols Number of colums for each keyboard row.
 * \param[out] reply JSON object with the reply, NULL on error.
 *
 * \return HTTP status code or -1 if the request was not completed.
 ****************************************************************************/
int bate_inline_keyboard_reply(const char *chat_id, const char *text,
		enum bate_text_mode parse_mode,
		const struct bate_inline_keyboard_button *key,
		unsigned int rows, const unsigned int *cols, cJSON **reply);

/************************************************************************//**
 * \brief Answer a previously received callback query.
 *
 * \param[in]  callback_query_id Identifier of the callback query to answer.
 * \param[in]  text Text for the notification to display.
 * \param[in]  show_alert If true shows an alert dialog instead of showing
 *             the text on top of the chat.
 * \param[in]  url URL that will be open by the user's client.
 * \param[in]  cache_time Maximum amount of seconds that the callback query
 *             result may be cached client-side. Defaults to 0.
 * \param[out] reply JSON object with the reply, NULL on error.
 *
 * \return HTTP status code or -1 if the request was not completed.
 ****************************************************************************/
int bate_callback_query_answer(const char *callback_query_id, const char *text,
		bool show_alert, const char *url, unsigned int cache_time,
		cJSON **reply);

/************************************************************************//**
 * \brief Sets a user defined event callback.
 *
 *  Setting an event callback is not needed. Just use it if you need an
 *  extra insight about what is happening under the hood.
 *
 * \param[in] event_cb Event callback function.
 ****************************************************************************/
void bate_event_cb_set(http_event_handle_cb event_cb);

/************************************************************************//**
 * \brief Gets the update_id from a single update (extracted from an array).
 *
 * \param[in] update JSON of a single update from the updates array.
 *
 * \return The update_id, or LONG_MIN on error.
 ****************************************************************************/
static inline long int bate_update_id_parse(cJSON *update)
{
	return json_get_long(update, "update_id");
}

/************************************************************************//**
 * \brief Gets the message_id from a message.
 *
 * \param[in] message JSON message object to parse.
 *
 * \return The message_id, or LONG_MIN on error.
 ****************************************************************************/
static inline long int bate_message_id_parse(cJSON *message)
{
	return json_get_long(message, "message_id");
}

/************************************************************************//**
 * \brief Cleans cached HTTP session resources.
 *
 * It is recommended calling this function only if you do not intend to use
 * this module anymore.
 ****************************************************************************/
void bate_cleanup(void);

//--------------- Very low level stuff beyond this point ---------------//
// Do not use these functions unless you need to do fancy stuff such as
// sending POST payloads in several chunks.
//----------------------------------------------------------------------//

/************************************************************************//**
 * \brief Prepare for issuing a POST request.
 *
 * * If write_length != 0, must be followed by bate_post_write()
 *   and bate_post_finish().
 * * If write_length == 0, must be followed by bate_post_finish().
 *
 * \param[in] method       Method with optional parameters when required. Must
 *            be null terminated. E.g. "sendPhoto".
 * \param[in] write_length Length of the data to send after this call.
 * \param[in] content_type Content-Type HTTP header for the data payload.
 *
 * \return 0 on success, non zero on error.
 ****************************************************************************/
int bate_post_init(const char *method, int write_length,
		const char *content_type);

/************************************************************************//**
 * \brief Write data to a initialized POST
 *
 * \param[in] data Data to send.
 * \param[in] length Length of the data to send.
 *
 * \return The number of bytes sent, -1 on error.
 ****************************************************************************/
int bate_post_write(const char *data, int length);

/************************************************************************//**
 * \brief Finish a POST request previously started with bate_post_init()
 *
 * \param[out] reply JSON object with the reply, NULL on error.
 *
 * \return HTTP status code or -1 if the request was not completed.
 ****************************************************************************/
int bate_post_finish(cJSON **reply);

/************************************************************************//**
 * \brief Initialize POST for sending a file to the Telegram bot.
 *
 * On success, this call must be followed by at least one call to
 * bate_post_write() and another to bate_file_send_finish().
 *
 * \param[in] chat_id     The chat identifier of the bot user.
 * \param[in] data_length Byte length of the file data.
 * \param[in] filename    Name of the file to report to the Telegram API.
 * \param[in] type        File type from the bate_file_type enum.
 * \param[in] mime        Mime type of the file from the bate_mime_type enum.
 *
 * \return The dynamically allocated content type used for the multipart
 * transfer. This is deallocated by the next bate_file_send_finish() call.
 * On error, NULL is returned.
 ****************************************************************************/
char *bate_file_send_init(const char *chat_id, size_t data_length,
		const char *filename, enum bate_file_type type,
		enum bate_mime_type mime);

/************************************************************************//**
 * \brief Finish a file send request previously started with
 * bate_file_send_init().
 *
 * \param[in]  content_type Content type returned by the call to
 *             bate_file_send_init().
 * \param[out] reply JSON object with the reply, NULL on error.
 *
 * \return HTTP status code or -1 if the request was not completed.
 ****************************************************************************/
int bate_file_send_finish(char *content_type, cJSON **reply);

#endif /*_BATE_H_*/

