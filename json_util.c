#include "json_util.h"

// Note: code does not have NULL checks, since the cJSON functions perform
// these checks themselves.

int json_get_bool(const cJSON *jobj, const char *name)
{
	cJSON *jaux = cJSON_GetObjectItem(jobj, name);

	if (!cJSON_IsBool(jaux)) {
		return -1;
	}

	return jaux->valueint;
}

long json_get_long(const cJSON *jobj, const char *name)
{
	cJSON *jaux = cJSON_GetObjectItem(jobj, name);

	if (!cJSON_IsNumber(jaux)) {
		return LONG_MIN;
	}

	return jaux->valuedouble;
}

const char *json_get_string(const cJSON *jobj, const char *name)
{
	cJSON *jaux = cJSON_GetObjectItem(jobj, name);

	if (!cJSON_IsString(jaux)) {
		return NULL;
	}

	return jaux->valuestring;
}

cJSON *json_get_object(const cJSON *jobj, const char *name)
{
	cJSON *jaux = cJSON_GetObjectItem(jobj, name);

	if (!cJSON_IsObject(jaux)) {
		return NULL;
	}

	return jaux;
}

cJSON *json_get_array(const cJSON *jobj, const char *name)
{
	cJSON *jaux = cJSON_GetObjectItem(jobj, name);

	if (!cJSON_IsArray(jaux)) {
		return NULL;
	}

	return jaux;
}

