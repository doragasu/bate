#
# Component Makefile
#
COMPONENT_ADD_INCLUDEDIRS := .
COMPONENT_SRCDIRS := .
COMPONENT_SUBMODULES := bate
COMPONENT_OBJS := bate.o json_util.o
# Embed the server root certificate into the final binary
COMPONENT_EMBED_TXTFILES := api_telegram_org_ca.pem
