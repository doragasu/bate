#include <esp_http_client.h>
#include <esp_system.h>
#include <esp_log.h>
#include <string.h>
#include "bate.h"

// Map ESP_LOGE() macros to something easier to use
#define LOGE(...) ESP_LOGE(__func__, __VA_ARGS__)
#define LOGD(...) ESP_LOGD(__func__, __VA_ARGS__)
#define LOGI(...) ESP_LOGI(__func__, __VA_ARGS__)
#define LOGW(...) ESP_LOGW(__func__, __VA_ARGS__)

// Wrappers used to extract different data types
#define long_get_(...)		json_get_long(__VA_ARGS__)
#define bool_get_(...)		json_get_bool(__VA_ARGS__)
#define object_get_(...)	json_get_object(__VA_ARGS__)
#define array_get_(...)		json_get_array(__VA_ARGS__)
static char *string_get_(const cJSON *jobj, const char *name)
{
	const char *aux = json_get_string(jobj, name);

	if (aux) {
		return strdup(aux);
	}

	return NULL;
}

// Wrappers used to free different data types
#define long_free_(mem)
#define bool_free_(mem)
#define object_free_(mem)
#define array_free_(mem)
#define string_free_(mem) free(mem)

// URL for the Telegram bot API
#define API_URL 	"https://api.telegram.org/bot" \
	CONFIG_TELEGRAM_BOT_TOKEN "/"
// Maximum length of the message payload
#define PAYLOAD_MAX	CONFIG_TELEGRAM_MSG_MAX_LEN
// Buffer length for requests (includes the API URL and the payload)
#define MSG_BUF_LEN	sizeof(API_URL) + PAYLOAD_MAX
// Offset of the request in the request buffer
#define REQ_OFF		(sizeof(API_URL) - 1)

// Variables used for the root cert needed by api.telegram.org
extern const char api_telegram_org_ca_start[]	\
	       asm("_binary_api_telegram_org_ca_pem_start");
extern const char api_telegram_org_ca_end[]	\
	       asm("_binary_api_telegram_org_ca_pem_end");

// Methods used for sending files
static const char * const file_post_method[BATE_FILE_MAX] = {
	"sendPhoto",
	"sendAudio",
	"sendDocument",
	"sendVideo",
	"sendAnimation",
	"sendVoice",
	"sendVideoNote",
	"sendMediaGroup"
};

static const char * const disposition_name[BATE_FILE_MAX] = {
	"photo",
	"audio",
	"document",
	"video",
	"animation",
	"voice",
	"video_note",
	"media"
};

static const char * const mime_str[BATE_MIME_MAX] = {
	"text/plain",
	"text/html",
	"image/jpeg",
	"image/png",
	"image/gif",
	"image/bmp",
	"video/x-msvideo",
	"video/mp4",
	"audio/x-wav",
	"audio/x-mpeg-3",
	"application/x-binary",
	"application/json"
};

// The client buffer
static esp_http_client_handle_t client = NULL;
// Request buffer
static char req_buf[MSG_BUF_LEN] = API_URL;
// Points to the begginning of the request (just past the API_URL)
static char * const req = req_buf + REQ_OFF;
// Event handler
static http_event_handle_cb client_event_cb = NULL;
// Connection flag
static int connected = false;


#define X_AS_OBJECT_PARSER(name, type_decl, type_name)	\
	data->name = type_name ## _get_(jaux, #name);

#define X_AS_DEALLOCATOR(name, type_decl, type_name)	\
	type_name ## _free_(data->name);

// Allocates the data buffer and reads the data. Appends a null termination
static char *adata_get(esp_http_client_handle_t client, int data_len)
{
	int last;
	int total;
	char *data = NULL;

	data = malloc(data_len + 1);
	if (!data) {
		goto out;
	}
	for (total = 0; total < data_len; total += last) {
		last = esp_http_client_read(client, data + total,
				data_len - total);
		if (last <= 0) {
			LOGE("data read failed");
			free(data);
			data = NULL;
			goto out;
		}
	}
	data[total] = '\0';

out:
	return data;
}

// Check if input reply_json is OK. if OK returns the input JSON. If not
// OK, deletes JSON and returns NULL.
static cJSON *check_reply_ok(cJSON *reply_json) {
	if (!reply_json) {
		goto out;
	}
	if (json_get_bool(reply_json, "ok") <= 0) {
		cJSON_Delete(reply_json);
		reply_json = NULL;
	}

out:
	return reply_json;
}

esp_err_t http_event_cb(esp_http_client_event_t *evt)
{
	switch(evt->event_id) {
		case HTTP_EVENT_ERROR:
			LOGD("HTTP_EVENT_ERROR");
			client = NULL;
			break;
		case HTTP_EVENT_ON_CONNECTED:
			connected = true;
			LOGD("HTTP_EVENT_ON_CONNECTED");
			break;
		case HTTP_EVENT_HEADER_SENT:
			LOGD("HTTP_EVENT_HEADER_SENT");
			break;
		case HTTP_EVENT_ON_HEADER:
			LOGD("HTTP_EVENT_ON_HEADER, key=%s, value=%s",
					evt->header_key, evt->header_value);
			break;
		case HTTP_EVENT_ON_DATA:
			LOGD("HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
			break;
		case HTTP_EVENT_ON_FINISH:
			LOGD("HTTP_EVENT_ON_FINISH");
			break;
		case HTTP_EVENT_DISCONNECTED:
			LOGD("HTTP_EVENT_DISCONNECTED");
			connected = false;
			break;
		default:
			LOGW("unhandled HTTP client event %d", evt->event_id);
	}

	// Call user defined callback function
	if (client_event_cb) {
		client_event_cb(evt);
	}
	return ESP_OK;
}

static int client_init(void)
{
	esp_http_client_config_t config = {
		.url = req_buf,
		.event_handler = http_event_cb,
		.cert_pem = api_telegram_org_ca_start,
		.timeout_ms = 65000,
	};
	client = esp_http_client_init(&config);

	if (!client) {
		return 1;
	}

	return 0;
}

int bate_get(const char *method, cJSON **reply)
{
	char *reply_str = NULL;
	esp_err_t err;
	int data_len;
	int status_code = -1;

	strncpy(req, method, PAYLOAD_MAX);
	if (!client) {
		err = client_init();
		if (err) {
			LOGE("client init failed");
			goto out;
		}
	}
	esp_http_client_set_url(client, req_buf);
	esp_http_client_set_method(client, HTTP_METHOD_GET);
	err = esp_http_client_open(client, 0);
	if (err != ESP_OK) {
		LOGE("open failed: %s", esp_err_to_name(err));
		goto out;
	}
	data_len = esp_http_client_fetch_headers(client);
	status_code = esp_http_client_get_status_code(client);
	if (!data_len) {
		LOGW("empty response");
		goto out;
	}
	reply_str = adata_get(client, data_len);
	if (reply) {
		*reply = reply_str ? cJSON_Parse(reply_str) : NULL;
		if (!*reply) {
			LOGE("failed to parse JSON: %s", reply_str ?
					reply_str : "(null)");
		}
	}

out:
	free(reply_str);
	return status_code;
}

int bate_post_init(const char *method, int write_length,
		const char *content_type)
{
	int err = 0;

	strncpy(req, method, PAYLOAD_MAX);
	
	if (!client) {
		err = client_init();
		if (err) {
			LOGE("client init failed");
			goto out;
		}
	}
	esp_http_client_set_url(client, req_buf);
	esp_http_client_set_method(client, HTTP_METHOD_POST);
	esp_http_client_set_header(client, "Content-Type", content_type);
	if (ESP_OK != esp_http_client_open(client, write_length)) {
		LOGE("open failed");
		err = 1;
	}

out:
	return err;
}

int bate_post_write(const char *data, int length)
{
	return esp_http_client_write(client, data, length);
}

int bate_post_finish(cJSON **reply)
{
	int data_len = 0;
	char *reply_str = NULL;
	int status_code = -1;

	data_len = esp_http_client_fetch_headers(client);
	status_code = esp_http_client_get_status_code(client);
	if (!data_len) {
		LOGW("empty response");
		goto out;
	}
	reply_str = adata_get(client, data_len);
	if (reply) {
		*reply = cJSON_Parse(reply_str);
		if (!*reply) {
			LOGE("failed to parse JSON: %s", reply_str);
		}
	}

out:
	if (reply_str) {
		free(reply_str);
	}
	return status_code;
}

int bate_post(const char *method, const char *data, int length,
		const char *content_type, cJSON **reply)
{
	int status_code = -1;
	int err;

	LOGD("method: %s", method);
	err = bate_post_init(method, length, content_type);
	if (err) {
		goto out;
	}

	if (data) {
		esp_http_client_write(client, data, length);
	}
	status_code = bate_post_finish(reply);

out:
	return status_code;
}

// Allocate and build the initial part for a multipart/form-data POST.
static int initial_part_alloc(const char *chat_id, const char *boundary,
		const char *name, const char *filename,
		const char *content_type, char **metadata)
{
	// Format of the json part and the header of the blob part:
	// - boundary + CRLF
	// - Content-Disposition: form-data; name="chat_id" + CRLFCRLF
	// - #chat_id + CRLF
	// - boundary + CRLF
	// - blob header + CRLFCRLF
	return asprintf(metadata, "--%s\r\n"
			"Content-Disposition: form-data; name=\"chat_id\"\r\n\r\n"
			"%s\r\n--%s\r\n"\
			"Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\n"
			"Content-Type: %s\r\n\r\n",
			boundary, chat_id, boundary, name, filename, content_type);
}

// Boundary consists of 24 dashes plus 16 random numbers
#define BOUNDARY_LEN		24 + 16
// Base to build the "Contenty-Type" header, with the multipart boundary
#define CONTENT_TYPE_BASE	"multipart/form-data; boundary=" \
	"------------------------"
#define CONTENT_TYPE_BASE_LEN	(sizeof(CONTENT_TYPE_BASE) - 1)
// Offset of the boundary in the CONTENT_TYPE_BASE definition
#define BOUNDARY_START_OFFSET	(CONTENT_TYPE_BASE_LEN - 24)
// Total length of the "Content-Type" header contents
#define CONTENT_TYPE_LEN 	CONTENT_TYPE_BASE_LEN + 16
char *bate_file_send_init(const char *chat_id, size_t data_length,
		const char *filename, enum bate_file_type type,
		enum bate_mime_type mime)
{
	char *content_type = NULL;
	char *boundary = NULL;
	int total_length;
	char *initial_part = NULL;
	int initial_part_length;
	int err;

	content_type = malloc(CONTENT_TYPE_LEN + 1);
	if (!content_type) {
		goto out;
	}
	strcpy(content_type, CONTENT_TYPE_BASE);
	// Reuse content_type buffer (by aliasing it at the correct offset)
	boundary = content_type + BOUNDARY_START_OFFSET;

	/// \todo It should be checked that boundary is not present
	/// in the data payload
	sprintf(boundary + 24, "%08"PRIx32"%08"PRIx32, esp_random(), esp_random());
	initial_part_length = initial_part_alloc(chat_id, boundary,
			disposition_name[type], filename, mime_str[mime],
			&initial_part);
	// total length includes:
	// - Initial part length
	// - Payload length + \r\n
	// - 4 + Boundary length + 2 + \r\n
	total_length = initial_part_length + data_length + 2 + 2 +
		BOUNDARY_LEN + 2 + 2;
	LOGD("initial length: %d, total length: %d, contents:\n%s",
			initial_part_length, total_length, initial_part);
	err = bate_post_init(file_post_method[type],
			total_length, content_type);
	if (err) {
		LOGE("client init failed");
		free(content_type);
		content_type = NULL;
		goto out;
	}

	// Send parts and closing boundary
	if (initial_part_length != esp_http_client_write(client, initial_part,
				initial_part_length)) {
		LOGE("initial data send failed");
		free(content_type);
		content_type = NULL;
		goto out;
	}

out:
	if (initial_part) {
		free(initial_part);
	}
	return content_type;
}

int bate_file_send_finish(char *content_type, cJSON **reply)
{
	int status_code = -1;
	const char *boundary;

	if (!content_type) {
		LOGE("missing content_type");
		goto out;
	}

	boundary = content_type + BOUNDARY_START_OFFSET;

	if ((4 != esp_http_client_write(client, "\r\n--", 4)) ||
	(BOUNDARY_LEN != esp_http_client_write(client, boundary,
		BOUNDARY_LEN)) ||
	(4 != esp_http_client_write(client, "--\r\n", 4))) {
		LOGE("ending data send failed");
		goto out;
	}

	// Finish and get reply
	status_code = bate_post_finish(reply);

out:
	free(content_type);
	return status_code;
}

int bate_file_send(const char *chat_id, const char *data, size_t length,
		const char *filename, enum bate_file_type type,
		enum bate_mime_type mime, cJSON **reply)
{
	char *content_type;
	int status_code = -1;

	// Initialize transfer
	content_type = bate_file_send_init(chat_id, length, filename,
			type, mime);
	if (!content_type) {
		goto out;
	}

	// Send payload
	if (length != esp_http_client_write(client, data, length)) {
		goto out;
	}

	// Finish and get reply
	status_code = bate_file_send_finish(content_type, reply);

out:
	return status_code;
}

void bate_event_cb_set(http_event_handle_cb event_cb)
{
	client_event_cb = event_cb;
}

struct bate_bot_info *bate_me_parse(const cJSON *json)
{
	cJSON *jaux;
	struct bate_bot_info *data = NULL;

	if (json_get_bool(json, "ok") <= 0) {
		LOGW("response not 'ok'");
		return NULL;
	}
	data = calloc(1, sizeof(struct bate_bot_info));

	jaux = cJSON_GetObjectItem(json, "result");
	BATE_BOT_INFO_TABLE(X_AS_OBJECT_PARSER);

	return data;
}

void bate_me_free(struct bate_bot_info *data)
{
	if (data) {
		BATE_BOT_INFO_TABLE(X_AS_DEALLOCATOR);
		free(data);
	}
}

int bate_updates_get(long offset, unsigned int timeout_s, cJSON **reply)
{
	char *method;
	int status_code = -1;

	asprintf(&method, "getUpdates?offset=%ld&allowed_updates=messages&"
			"timeout=%u", offset, timeout_s);

	status_code = bate_get(method, reply);
	free(method);

	if (reply) {
		*reply = check_reply_ok(*reply);
		if (!*reply) {
			status_code = -1;
		}
	}

	return status_code;
}

static int msg_send_internal(cJSON *json, const char *chat_id,
		const char *text, enum bate_text_mode parse_mode, cJSON **reply)
{
	char *parse_mode_str;
	int status_code = -1;
	char *msg;

	switch (parse_mode) {
		case BATE_TEXT_HTML:
			parse_mode_str = "HTML";
			break;

		case BATE_TEXT_MARKDOWN:
			parse_mode_str = "Markdown";
			break;
	
		default:
			parse_mode_str = NULL;
			break;
	};

	cJSON_AddRawToObject(json, "chat_id", chat_id);
	if (parse_mode_str) {
		cJSON_AddStringToObject(json, "parse_mode", parse_mode_str);
	}
	cJSON_AddStringToObject(json, "text", text);

	msg = cJSON_PrintUnformatted(json);

	status_code = bate_post("sendMessage", msg, strlen(msg),
			"application/json", reply);
	free(msg);

	if (reply) {
		*reply = check_reply_ok(*reply);
		if (!*reply) {
			status_code = -1;
		}
	}

	return status_code;
}

int bate_msg_send(const char *chat_id, const char *text,
		enum bate_text_mode parse_mode, cJSON **reply)
{
	int status_code;
	cJSON *json = cJSON_CreateObject();

	status_code = msg_send_internal(json, chat_id, text, parse_mode, reply);
	cJSON_Delete(json);

	return status_code;
}

int bate_keyboard_reply(const char *chat_id, const char *text,
		enum bate_text_mode parse_mode, const char **key,
		unsigned int rows, const unsigned int *cols,
		enum bate_keyboard_flags flags, cJSON **reply)
{
	int status_code;
	cJSON *json = cJSON_CreateObject();
	cJSON *markup_obj, *keyb_array, *row_array;

	markup_obj = cJSON_CreateObject();
	cJSON_AddItemToObject(json, "reply_markup", markup_obj);
	if (rows) {
		keyb_array = cJSON_CreateArray();
		cJSON_AddItemToObject(markup_obj, "keyboard", keyb_array);
		for (int i = 0, row = 0; row < rows && key[i]; row++) {
			row_array = cJSON_CreateArray();
			cJSON_AddItemToArray(keyb_array, row_array);
			for (int j = 0; j < cols[row]; j++, i++) {
				cJSON_AddStringToObject(row_array, "text",
						key[i]);
			}
		}
		// Add keyboard flags
		if (flags & BATE_KEYB_RESIZE) {
			cJSON_AddBoolToObject(markup_obj, "resize_keyboard",
					true);
		}
		if (flags & BATE_KEYB_ONE_TIME) {
			cJSON_AddBoolToObject(markup_obj, "one_time_keyboard",
					true);
		}
		if (flags & BATE_KEYB_SELECTIVE) {
			cJSON_AddBoolToObject(markup_obj, "selective", true);
		}
	}

	status_code = msg_send_internal(json, chat_id, text, parse_mode, reply);
	cJSON_Delete(json);

	return status_code;
}

// Wrappers used for adding fields to JSON object
#define json_add_string_(obj, name, string)	\
	cJSON_AddStringToObject(obj, name, string)
#define json_add_bool_(obj, name, bool_var)	\
	cJSON_AddBoolToObject(obj, name, bool_var)

/// Expand table as adder functions for every inline keyboard button type
#define X_AS_ADD_INLINE_FIELD_FUNCT(u_name, type_name, type_decl, l_name)   \
	static void add_inline_ ## l_name(cJSON *btn,			    \
			const struct bate_inline_keyboard_button *key) {    \
		json_add_ ## type_name ## _(btn, #l_name, key->l_name);     \
	}

/// Adder functions for every inline keyboard button type
INLINE_KEYB_BTN_TYPE_TABLE(X_AS_ADD_INLINE_FIELD_FUNCT);

/// Expand as lookup table for adder functions
#define X_AS_ADD_INLINE_FIELD_FUNCT_LUT(u_name, type_name, type_decl, l_name) \
	add_inline_ ## l_name,

/// Lookup table for adder functions
static void (* const add_inline[BATE_INLINE_KEYB_TYPE_MAX])(cJSON*,
		const struct bate_inline_keyboard_button*) = {
	INLINE_KEYB_BTN_TYPE_TABLE(X_AS_ADD_INLINE_FIELD_FUNCT_LUT)
};

/// Add an inline button
static void add_inline_button(cJSON *row_array,
		const struct bate_inline_keyboard_button *key)
{
	cJSON *btn_obj = cJSON_CreateObject();

	cJSON_AddItemToArray(row_array, btn_obj);
	cJSON_AddStringToObject(btn_obj, "text", key->text);
	add_inline[key->type](btn_obj, key);
}

int bate_inline_keyboard_reply(const char *chat_id, const char *text,
		enum bate_text_mode parse_mode,
		const struct bate_inline_keyboard_button *key,
		unsigned int rows, const unsigned int *cols, cJSON **reply)
{
	int status_code;
	cJSON *json = cJSON_CreateObject();
	cJSON *markup_obj, *keyb_array, *row_array;

	markup_obj = cJSON_CreateObject();
	cJSON_AddItemToObject(json, "reply_markup", markup_obj);
	if (rows) {
		keyb_array = cJSON_CreateArray();
		cJSON_AddItemToObject(markup_obj, "inline_keyboard", keyb_array);
		for (int i = 0, row = 0; row < rows && key[i].text; row++) {
			row_array = cJSON_CreateArray();
			cJSON_AddItemToArray(keyb_array, row_array);
			for (int j = 0; j < cols[row]; j++, i++) {
				add_inline_button(row_array, &key[i]);
			}
		}
	}

	status_code = msg_send_internal(json, chat_id, text, parse_mode, reply);
	cJSON_Delete(json);

	return status_code;
}

int bate_callback_query_answer(const char *callback_query_id, const char *text,
		bool show_alert, const char *url, unsigned int cache_time,
		cJSON **reply)
{
	cJSON *json;
	int status_code = -1;
	char *msg = NULL;

	if (!callback_query_id) {
		goto out;
	}

	json = cJSON_CreateObject();

	cJSON_AddStringToObject(json, "callback_query_id", callback_query_id);
	if (text) {
		cJSON_AddStringToObject(json, "text", text);
	}
	if (show_alert) {
		cJSON_AddBoolToObject(json, "show_alert", true);
	}
	if (url) {
		cJSON_AddStringToObject(json, "url", url);
	}
	if (cache_time) {
		cJSON_AddNumberToObject(json, "cache_time", cache_time);
	}

	msg = cJSON_PrintUnformatted(json);
	cJSON_Delete(json);

	status_code = bate_post("answerCallbackQuery", msg, strlen(msg),
			"application/json", reply);

out:
	if (msg) {
		free(msg);
	}
	return status_code;
}

long int bate_update_id(cJSON *update)
{
	long id = -1;
	cJSON *jobj = cJSON_GetObjectItem(update, "update_id");

	if (cJSON_IsNumber(jobj)) {
		return jobj->valuedouble;
	}

	return id;
}

struct bate_update *bate_update_parse(const cJSON *update)
{
	struct bate_update *data = NULL;
	const cJSON *jaux = update;

	if (jaux) {
		data = calloc(1, sizeof(struct bate_update));
		data->update_id = json_get_long(jaux, "update_id");
		if ((data->message = json_get_object(jaux, "message"))) {
			data->type = BATE_UPDATE_TYPE_MESSAGE;
		} else if ((data->callback_query = json_get_object(jaux,
						"callback_query"))) {
			data->type = BATE_UPDATE_TYPE_CALLBACK_QUERY;
		}
	}

	return data;
}

void bate_update_free(struct bate_update *data)
{
	free(data);
}

struct bate_message *bate_message_parse(const cJSON *message)
{
	struct bate_message *data = NULL;
	const cJSON *jaux = message;

	if (jaux) {
		data = calloc(1, sizeof(struct bate_message));
		BATE_MESSAGE_TABLE(X_AS_OBJECT_PARSER);
	}

	return data;
}

void bate_message_free(struct bate_message *data)
{
	if (data) {
		BATE_MESSAGE_TABLE(X_AS_DEALLOCATOR);
		free(data);
	}
}

struct bate_from *bate_from_parse(const cJSON *from)
{
	struct bate_from *data = NULL;
	const cJSON *jaux = from;

	if (jaux) {
		data = calloc(1, sizeof(struct bate_from));
		BATE_FROM_TABLE(X_AS_OBJECT_PARSER);
	}

	return data;
}

void bate_from_free(struct bate_from *data)
{
	if (data) {
		BATE_FROM_TABLE(X_AS_DEALLOCATOR);
		free(data);
	}
}

struct bate_chat *bate_chat_parse(const cJSON *chat)
{
	struct bate_chat *data = NULL;
	const cJSON *jaux = chat;

	if (jaux) {
		data = calloc(1, sizeof(struct bate_chat));
		BATE_CHAT_TABLE(X_AS_OBJECT_PARSER);
	}

	return data;
}

void bate_chat_free(struct bate_chat *data)
{
	if (data) {
		BATE_CHAT_TABLE(X_AS_DEALLOCATOR);
		free(data);
	}
}

struct bate_entity *bate_entity_parse(const cJSON *entity)
{
	struct bate_entity *data = NULL;
	const cJSON *jaux = entity;

	if (jaux) {
		data = calloc(1, sizeof(struct bate_entity));
		BATE_ENTITY_TABLE(X_AS_OBJECT_PARSER);
	}

	return data;
}

void bate_entity_free(struct bate_entity *data)
{
	if (data) {
		BATE_ENTITY_TABLE(X_AS_DEALLOCATOR);
		free(data);
		}
}

struct bate_callback_query *bate_callback_query_parse(const cJSON *entity)
{
	struct bate_callback_query *data = NULL;
	const cJSON *jaux = entity;

	if (jaux) {
		data = calloc(1, sizeof(struct bate_callback_query));
		BATE_CALLBACK_QUERY_TABLE(X_AS_OBJECT_PARSER);
	}

	return data;
}

void bate_callback_query_free(struct bate_callback_query *data)
{
	if (data) {
		BATE_CALLBACK_QUERY_TABLE(X_AS_DEALLOCATOR);
		free(data);
	}
}

void bate_cleanup(void)
{
	if (client) {
		esp_http_client_cleanup(client);
		client = NULL;
	}
}
