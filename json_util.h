#ifndef _JSON_UTIL_H_
#define _JSON_UTIL_H_

#include <cJSON.h>
#include <limits.h>

/************************************************************************//**
 * Get a bool from a JSON object.
 *
 * \param[in] jobj JSON object to search for a bool.
 * \param[in] name Name of the bool to search.
 *
 * \return The bool contents, or -1 if it was not found.
 ****************************************************************************/
int json_get_bool(const cJSON *jobj, const char *name);

/************************************************************************//**
 * Get a long from a JSON object.
 *
 * \param[in] jobj JSON object to search for a long.
 * \param[in] name Name of the long to search.
 *
 * \return The long contents, or LONG_MIN if it was not found.
 ****************************************************************************/
long json_get_long(const cJSON *jobj, const char *name);

/************************************************************************//**
 * Get a string from a JSON object.
 *
 * \param[in] jobj JSON object to search for a string.
 * \param[in] name Name of the string to search.
 *
 * \return The string contents, or NULL if it was not found.
 * \note The returned string is valid until jobj is freed with cJSON_Delete().
 ****************************************************************************/
const char *json_get_string(const cJSON *jobj, const char *name);

/************************************************************************//**
 * Get an object from a JSON object.
 *
 * \param[in] jobj JSON object to search for an object.
 * \param[in] name Name of the object to search.
 *
 * \return The requested object, or NULL if it was not found.
 * \note The returned object is valid until jobj is freed with cJSON_Delete().
 ****************************************************************************/
cJSON *json_get_object(const cJSON *jobj, const char *name);

/************************************************************************//**
 * Get an array from a JSON object.
 *
 * \param[in] jobj JSON object to search for an array.
 * \param[in] name Name of the array to search.
 *
 * \return The requested array, or NULL if it was not found.
 * \note The returned array is valid until jobj is freed with cJSON_Delete().
 ****************************************************************************/
cJSON *json_get_array(const cJSON *jobj, const char *name);

#endif /*_JSON_UTIL_H_*/

