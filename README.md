# Bot API for Telegram on ESP (BATE)

BATE implements a simple API allowing to easily build Telegram bots. The library is targeted at Espressif platforms. It can run on the ESP8266 and ESP32 chips. Its footprint is very small, and uses esp_http_client component to implement the REST API calls.

BATE lacks many advanced functions, but in its current state, it allows:

* Receiving message updates
* Sending messages
* Sending keyboards
* Sending files (as document, photograph, video, etc.).
* Parsing the response JSON messages as easy to access structures.

# Usage

BATE requires using [esp-idf](https://github.com/espressif/esp-idf) for ESP32 chips, or [ESP8266_RTOS_SDK](https://github.com/espressif/ESP8266_RTOS_SDK) for ESP8266 chips. To use BATE as a component, just clone this repository under the `components` directory of your project.

Once cloned, when you run `make menuconfig`, under `Component options` menu will appear a `Telegram bot` submenu, allowing to configure parameters such as the bot token. Set the parameters as needed, `#include <bate.h>` and you are done!

The [bate.h](bate.h) header file is heavily commented, including Doxygen tags. Just browse it or see an usage example [here](https://gitlab.com/doragasu/bate_test). I will also generate the Doxygen documentation as soon as I get some time ^_^.

Enjoy!

# License

BATE is licensed under the Mozilla Public License (MPL) 2.0. You can browse the license [here](https://www.mozilla.org/en-US/MPL/2.0/).
